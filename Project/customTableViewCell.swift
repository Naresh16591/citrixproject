//
//  customTableViewCell.swift
//  Project
//
//  Created by Naresh Kumar Nagulavancha on 6/28/16.
//  Copyright © 2016 Incomm. All rights reserved.
//

import UIKit

class customTableViewCell: UITableViewCell{


	@IBOutlet weak var header: UILabel!
	@IBOutlet weak var details: UILabel!

	override func awakeFromNib() {
		super.awakeFromNib()
	}

	override func setSelected(selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}

	func updateViewAtIndex(heading headerText:String, details values: AnyObject){
		header.text = headerText
		if values is String {
			details.text = values as? String
		}

		if values is NSArray {
			let modifiedValues = (values as? NSArray) as Array?
			var str = ""
			var count = 0
			if let modValues = modifiedValues{
				for i in modValues{
					if i is String{
						if let i  = i as? String {
							if count != modValues.count-1 {
								str += i + "\n"
							}
							else {
								str += i
							}
							count = count+1
						}
					}
				}
			}
			print(str)

			details.numberOfLines = 0
			details.text = str
			
		}

	}

}
