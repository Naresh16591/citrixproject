//
//  ContactDetailTableViewController.swift
//  Project
//
//  Created by Naresh Kumar Nagulavancha on 6/28/16.
//  Copyright © 2016 Incomm. All rights reserved.
//

import UIKit


let contactDetailTableViewCellIdentifier = "customCell"
enum Headers:String {
	case companyName = "companyName"
	case name = "name"
	case parent = "parent"
	case manager = "managers"
	case addresses = "addresses"
	case phones = "phones"
}

class ContactDetailTableViewController: UITableViewController {

	var dictionary: NSDictionary? = nil
	var dictionaryArray:[String]? = nil

	override func viewDidLoad() {
		super.viewDidLoad()
		processData()
		tableView.reloadData()
		configureTableView()
	}

	func configureTableView() {
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 77
	}

	//Mark: Get all keys in dictionary

		func processData(){

			//dispatch_async(<#T##queue: dispatch_queue_t##dispatch_queue_t#>, <#T##block: dispatch_block_t##dispatch_block_t##() -> Void#>)
			if let dictionary = self.dictionary {
			let tempArray = Array(dictionary.allKeys).map({($0 as? String)!})

			print(tempArray)

			if dictionaryArray == nil {
				dictionaryArray = []
			}

			// Sicne dictionary is unorderd data, trying Ordering the data as much as i can.
			if tempArray.contains(Headers.companyName.rawValue){
				dictionaryArray?.append(Headers.companyName.rawValue)
			}
			if tempArray.contains(Headers.name.rawValue){
				dictionaryArray?.append(Headers.name.rawValue)
			}
			if tempArray.contains(Headers.parent.rawValue){
				dictionaryArray?.append(Headers.parent.rawValue)
			}
			if tempArray.contains(Headers.manager.rawValue){
				dictionaryArray?.append(Headers.manager.rawValue)
			}
			if tempArray.contains(Headers.addresses.rawValue){
				dictionaryArray?.append(Headers.addresses.rawValue)
			}
			if tempArray.contains(Headers.phones.rawValue){
				dictionaryArray?.append(Headers.phones.rawValue)
			}
			// If headers are other headers, filter and append to dictionaryArray

			for i in tempArray {
				if !dictionaryArray!.contains(i) {
					dictionaryArray?.append(i)
				}
			}

			print(dictionaryArray)

		}


	}

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let dictionaryArray = self.dictionaryArray {
			return dictionaryArray.count
		}
		return 0
	}
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	//Mark: Configure cell

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(contactDetailTableViewCellIdentifier, forIndexPath: indexPath) as? customTableViewCell
		if let dictionary = self.dictionary, keys = self.dictionaryArray{
			if let value = dictionary[keys[indexPath.row]]{
				cell?.updateViewAtIndex(heading: keys[indexPath.row], details: value)
			}
		}
		return cell!

	}



}
