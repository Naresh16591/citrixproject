//
//  ContactsTableViewController.swift
//  Project
//
//  Created by Naresh Kumar Nagulavancha on 6/28/16.
//  Copyright © 2016 Incomm. All rights reserved.
//

import UIKit

let dataURL = "https://api.myjson.com/bins/4tf1w"
let detailViewControllerIdentifier = "detailView"
let contacTableViewCellIdentifier = "cell"

class ContactsTableViewController: UITableViewController {

	var listOfContacts: [NSDictionary]? = nil
	var displayContats: [NSDictionary]? = nil

	override func viewDidLoad() {

		loadItems()

		// Configure searchBar

		self.enableSearchBarInTableViewHeader(withDelegate: self)


	}

	//Mark: Network call

	func loadItems() {

		MBProgressHUD.showHUDAddedTo(self.tableView, animated: true)

		let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
		let session = NSURLSession(configuration: configuration)
		guard let url = NSURL(string: dataURL) else { return}
		let task = session.dataTaskWithURL(url){
			data, response, error in
			if let data = data{
				do {
					let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
					let contacts = json["contacts"] as? [NSDictionary]
					dispatch_async(dispatch_get_main_queue()){
						MBProgressHUD.hideHUDForView(self.tableView, animated: true)
						self.listOfContacts = contacts
						self.displayContats = self.listOfContacts
						self.tableView.reloadData()
					}
				}
				catch {
					print("Something went wrong")
				}
			}

		}
		task.resume()
	}


	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let displayContats = displayContats{
			return displayContats.count
		}
		return 0
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		let cell = tableView.dequeueReusableCellWithIdentifier(contacTableViewCellIdentifier, forIndexPath: indexPath)

		if let displayContats = displayContats{
			if let text = displayContats[indexPath.row]["companyName"] as? String {
				cell.textLabel?.text = text
			}

			if let text = displayContats[indexPath.row]["name"] as? String {
				cell.textLabel?.text = text
			}
		}
		return cell
	}

	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if let displayContats = self.displayContats{
				let storyBoard = UIStoryboard(name: "Main", bundle:nil)
				let contactDetailViewController = storyBoard.instantiateViewControllerWithIdentifier(detailViewControllerIdentifier) as? ContactDetailTableViewController
				contactDetailViewController?.dictionary = displayContats[indexPath.row]
				self.navigationController?.pushViewController(contactDetailViewController!, animated: true)
		}
	}
}

//Mark: Search Delegate Implementation

extension ContactsTableViewController: UISearchBarDelegate{

	func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
		self.handleSearchBarBeginEdit(searchBar)
		return true
	}

	func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText.characters.count == 0
		{
			self.displayContats = self.listOfContacts
		}
		else
		{
			self.displayContats = self.listOfContacts!.filter({
				if let text = $0["companyName"] as? String {
					return text.lowercaseString.containsString(searchText.lowercaseString)
				}
				if let text = $0["name"] as? String {
					return text.lowercaseString.containsString(searchText.lowercaseString)
				}
				return false
				}
			)
		}

		self.tableView.reloadData()
	}

	func searchBarCancelButtonClicked(searchBar: UISearchBar) {
		self.handleSearchBarCancelButtonTapped(searchBar)
		self.displayContats = listOfContacts
		self.tableView.reloadData()
	}

}
