//
//  TableViewController.swift
//  Project
//
//  Created by Naresh Kumar Nagulavancha on 6/28/16.
//  Copyright © 2016 Incomm. All rights reserved.
//

import UIKit

extension UITableViewController
{
	func enableSearchBarInTableViewHeader(withDelegate delegate: UISearchBarDelegate)
	{
		let searchBar = UISearchBar()
		searchBar.delegate = delegate
		searchBar.searchBarStyle = .Minimal
		searchBar.sizeToFit()
		self.tableView.tableHeaderView = searchBar
	}

	func handleSearchBarBeginEdit(searchBar: UISearchBar) -> Bool
	{
		searchBar.setShowsCancelButton(true, animated: true)
		return true
	}

	func handleSearchBarCancelButtonTapped(searchBar: UISearchBar)
	{
		searchBar.setShowsCancelButton(false, animated: true)
		searchBar.text = nil
		searchBar.resignFirstResponder()
	}

	var searchBar: UISearchBar?
	{
		if let bar = self.tableView.tableHeaderView as? UISearchBar
		{
			return bar
		}
		return nil
	}
}